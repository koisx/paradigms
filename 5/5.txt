primes :: [Integer]
primes = sieve (2 : [3*3, 5*5..])
  where
    sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]

main = do
  let num = 5
  let pr = primes
  let n = num *2
  let res = take n pr
  print res