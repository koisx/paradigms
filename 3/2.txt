main = do
  let n = 4;
  let x = [2*k+1 | k<-[1..n], k>=1, k<=n]
  print x